This repository contains exercises I did for the Rockeseat Bootcamp, it's focused on
JavaScript, React.JS, React Native and a tiny bit of node.
The structure will be divided into the modules that were created by Rocketseat, in each
modules' folder, it will contain the codes I got from the classes and the exercises I did while progressing in the bootcamp.

Cheers.

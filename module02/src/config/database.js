module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: '179014',
  database: 'goBarber',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
